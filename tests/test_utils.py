import pytest
from unittest.mock import MagicMock, patch, call
from utils.utils import write_to_file


@pytest.mark.parametrize('name, phrase',
                         [('file1', 'phrase1'),
                          ('file2', 'phrase2'),
                          ('file3', 'phrase3')])
def test_write_to_file_mock(name, phrase):
    with patch("builtins.open", MagicMock()) as mock:
        write_to_file(name, phrase)
        mock.assert_called()
        mock.assert_called_with(f'{name}.txt', 'w+')
        mock.assert_has_calls(calls=[call(f'{name}.txt', 'w+')])


@pytest.mark.parametrize('name, phrase',
                         [(55, 'phrase1'),
                          ('file2', 3.8)])
def test_write_to_file_errors(name, phrase):
    with pytest.raises(TypeError):
        write_to_file(name, phrase)


def test_write_to_file_magic_mock_fixture(fixture_for_hw):
    with fixture_for_hw as mock:
        write_to_file('some_name', 'some_phrase')
        mock.assert_called()
        mock.assert_called_with('some_name.txt', 'w+')


