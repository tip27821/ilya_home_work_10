
class WriteManager:
    def __init__(self, name, flags='w'):
        self.obj = open(name, flags)

    def __enter__(self):
        return self.obj

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            print('Error!')
            return True
        self.obj.close()


def write_to_file(name,phrase):
    if type(name) != str or type(phrase) != str:
        raise TypeError('Type Error!\nПожалуйста введите нужный тип данных!')
    else:
        pass
    with WriteManager(f'{name}.txt', 'w+') as w:
        w.write(phrase)


#name = input('Введите название файла: ')
#phrase = input('Введите желаемую фразу: ')

#write_to_file(name, phrase)

